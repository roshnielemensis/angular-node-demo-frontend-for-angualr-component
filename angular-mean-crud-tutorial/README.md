# angular node demo frontend for Angualr Component

## Create Angular Project

### Generate the new Angular project

```terminal
ng new angular-mean-crud-tutorial
```

### Move inside the project root

```terminal
cd angular-mean-crud-tutorial
```

### install Bootstrap package

```terminal
npm install bootstrap
```

### Place Bootstrap CSS path in angular.json file

```json
"styles": [
     "src/styles.scss",
     "node_modules/bootstrap/dist/css/bootstrap.min.css"
]
```

## Create Components in Angular

Execute command to generate a couple of components that will be used for the Angular 13 CRUD project.

```terminal
ng g c components/add-book
ng g c components/book-detail
ng g c components/books-list
```

You have just generated the add-book, book-detail, and books-list components folder.

## Create CRUD Routes

Next, create routes; with the help of Angular 13 routes, we will make the consensus with components to enable the navigation in the CRUD application so add the below code in the **app-routing.module.ts** file.

```routes
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'add-book' },
  { path: 'books-list', component: BooksListComponent },
  { path: 'add-book', component: AddBookComponent },
  { path: 'edit-book/:id', component: BookDetailComponent }
];
```

### Import HttpClientModule, FormsModule & ReactiveFormsModule

HTTP requests are made to send and update the server’s data.
 It requires working on Form data and HTTP calls; consequently, 
 we need to import and register HttpClientModule, FormsModule and ReactiveFormsModule in the **app.module.ts** file.

 ```ts
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
 imports: [
     HttpClientModule,
    FormsModule,
    ReactiveFormsModule
 ]
```

## Create Angular Service for REST API Consumption

Theoretically, we need to keep the Angular Service and Model in a separate folder, so create **app/service** folder in Angular project and create **Book.ts** class within

Then, add the below code in **app/service/Book.ts** file.

```ts
export class Book {
    _id!: String;
    name!: String;
    price!: String;
    description!: String;
}
```

### Create Service File

```terminal
ng g s service/crud
```

Then, add the below code in **app/service/crud.service.ts** file

```export class CrudService {
  // Node/Express API
  REST_API: string = 'http://localhost:8000/api';
  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private httpClient: HttpClient) {}
  // Add
  AddBook(data: Book): Observable<any> {
    let API_URL = `${this.REST_API}/add-book`;
    return this.httpClient
      .post(API_URL, data)
      .pipe(catchError(this.handleError));
  }
  // Get all objects
  GetBooks() {
    return this.httpClient.get(`${this.REST_API}`);
  }
  // Get single object
  GetBook(id: any): Observable<any> {
    let API_URL = `${this.REST_API}/read-book/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders }).pipe(
      map((res: any) => {
        return res || {};
      }),
      catchError(this.handleError)
    );
  }
  // Update
  updateBook(id: any, data: any): Observable<any> {
    let API_URL = `${this.REST_API}/update-book/${id}`;
    return this.httpClient
      .put(API_URL, data, { headers: this.httpHeaders })
      .pipe(catchError(this.handleError));
  }
  // Delete
  deleteBook(id: any): Observable<any> {
    let API_URL = `${this.REST_API}/delete-book/${id}`;
    return this.httpClient
      .delete(API_URL, { headers: this.httpHeaders })
      .pipe(catchError(this.handleError));
  }
  // Error
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      errorMessage;
    });
  }
}
```

## Adding Navigation with Bootstrap

To configure navigation define the router-outlet directive, routerLink directive with angular routes in **app.component.html** file

```ts
 <ul class="navbar-nav ml-auto ">
      <li class="nav-item">
        <a class="nav-link" routerLinkActive="active" routerLink="/books-list">Show Books</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" routerLinkActive="active" routerLink="/add-book">Add Books</a>
      </li>
    </ul>
```

## Build Create Operation

This step comprises of creating or adding data to the MongoDB database using Angular and Node/Express REST API.

Add code in **add-book.component.ts** file
Add code in **add-book.component.html** file

## Render Data Object and Show as a List

The Book objects are being fetched from the database using the CrudService; simultaneously,
 a single book object is being deleted using the **delete()** method.

 Add code in **books-list.component.ts** file.
 Add code in **books-list.component.html** file.

## Create and Edit Details Page

Generically, we are using ActivatedRoute router API to get the object ID from the URL; based on the key,
 we are accessing the GetBook() method to fetch the book object.
 Also, using the updateBook method to update the data on the database.

  Add code in **books-detail.component.ts** file.
   Add code in **books-detail.component.html** file.

### Finally, start the app development server of angular application

```terminal
ng serve --open
```
